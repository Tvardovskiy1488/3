public class Array {

    public static void main(String[] args) {
        System.out.println("Исходный массив: ");
        double[] Array = new double[10];
        for (int i = 0; i < Array.length; i++) {
            Array[i] = (int) Math.round(Math.random() * 9);
            System.out.println( Array[i] );
        }

        double a = 1.1;
        for (int i = 0; i <Array.length; i++){
            Array[i] = Array[i] * a;
        }



        boolean isSorted = false; //Метод "Пузырька"
        double buf;
        while(!isSorted) {
            isSorted = true;
            for (int i = 0; i < Array.length-1; i++) {
                if(Array[i] < Array[i+1]){
                    isSorted = false;

                    buf = Array[i];
                    Array[i] = Array[i+1];
                    Array[i+1] = buf;
                }
            }
        }



        System.out.println ("Конечный массив: ");
        for (int i = 0; i < 10; i++) {
            System.out.print (Array[i] + "    "); // Выводим на экран, полученный массив
        }
        System.out.println();
    }
}
